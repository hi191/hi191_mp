<!DOCTYPE html>
<html>
<head>
<style>
	h1.main_message{ 
		padding-left: 25px
	}
	p.sub_message{ padding-left: 26px }
</style>
<title>Not Logged In</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/paper/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<link rel="stylesheet" href="<?=base_url()?>assets/css/homestyle.css">
</head>
<body>

<h1 class = "main_message">Not Logged In</h1>
<p class = "sub_message">You must be logged in to access this part. Please login at <a href = "<?=site_url()?>login">Login Page</p>

</body>
</html>
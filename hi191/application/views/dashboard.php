<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>HI 191 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?=base_url();?>/assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=base_url();?>/assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?=base_url();?>/assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=base_url();?>/assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
        <link rel="stylesheet" href="<?=base_url();?>/assets/adminlte/dist/css/skins/skin-black.min.css">
        <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo.png">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!--
BODY TAG OPTIONS:
=================
Apply one or more of the following classes to get the
desired effect
|---------------------------------------------------------|
| SKINS         | skin-blue                               |
|               | skin-black                              |
|               | skin-purple                             |
|               | skin-yellow                             |
|               | skin-red                                |
|               | skin-green                              |
|---------------------------------------------------------|
|LAYOUT OPTIONS | fixed                                   |
|               | layout-boxed                            |
|               | layout-top-nav                          |
|               | sidebar-collapse                        |
|               | sidebar-mini                            |
|---------------------------------------------------------|
-->
<body class="hold-transition skin-black sidebar-mini">
  <div class="wrapper">

    <!-- Main Header -->
    <header class="main-header">

      <!-- Logo -->
      <a href="<?=site_url()?>dashboard" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">HI191</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">HI 191</span>
      </a>

      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <!-- <img src= "<?=base_url();?>/assets/images/<?=$_SESSION['First_Name']?>.jpg" class="user-image" alt="User Image"> -->
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <!-- <img src="<?=base_url();?>/assets/images/<?=$_SESSION['First_Name']?>.jpg" class="img-circle" alt="User Image"> -->
                  <p>
                   <!-- <?=$_SESSION['First_Name']." ".$_SESSION['Last_Name']?> -->
                   <!-- <small><?=$_SESSION['Admin_Type']?></small> -->
                 </p>
               </li>
               <!-- Menu Body -->
               <!-- Menu Footer-->
               <li class="user-footer">
                <div class="pull-right">
                  <!-- <a href="<?=site_url()?>login/logout_user" class="btn btn-default btn-flat">Sign out</a> -->
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
      </ol> -->
    </section>

    <!-- Main content -->
    <section class="content container-fluid">
      <div class="modal fade" id="modal-default">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Results Table</h4>
              </div>
              <div class="modal-body">
                <div id="modal_loader" style="font-size:7em;text-align: center; ">
                  <i style="color:#000000;" class="fa fa-spinner fa-spin"></i>
                </div>
                <table id="data" class="table table-bordered"></table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title"><a id="q1_table" href="#" data-toggle="modal" data-target="#modal-default">Patients' Readmission Ratio</a></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="row">
                  <div class="col s4">
                    <p class="text-center">
                      <strong>Patients Readmitted Between 30 days vs Non-readmitted Patients</strong>
                    </p>

                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                      <canvas id="question1_chart"></canvas>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title"><a href="#" id="q2_table" href="#" data-toggle="modal" data-target="#modal-default">Number of Patients within 30 Days by Sex</a></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="row">
                  <div class="col s4">
                    <p class="text-center">
                      <strong>Number of Patients Readmitted within 30 Days Grouped by Sex</strong>
                    </p>

                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                      <canvas id="question2_chart"></canvas>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title"><a id="q3_table" href="#" data-toggle="modal" data-target="#modal-default">Number of Patients per Age Group</a></h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="row">
                  <div class="col s4">
                    <p class="text-center">
                      <strong>Number of Patients per Age Group</strong>
                    </p>

                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                      <canvas id="question3_chart" style="padding-left: 35px; max-width: 95%;"></canvas>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
          <div class="col-md-6 col-sm-12">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title"><a href="#" id="q4_table" href="#" data-toggle="modal" data-target="#modal-default">Number of Patients Grouped according to Days Confined</a></h3>
                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="row">
                  <div class="col s4">
                    <p class="text-center">
                      <strong>Number of Patients Grouped according to Days Confined</strong>
                    </p>

                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                      <canvas id="question4_chart" style="padding-left: 35px; max-width: 95%;"></canvas>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>
        </div>

        <div class="row">
          <div class="col-md-6 col-sm-12">
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title"><a href="#" id="q5_table" href="#" data-toggle="modal" data-target="#modal-default">Top 5 1st Diagnosis</a></h3>

                <div class="box-tools pull-right">
                  <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="row">
                  <div class="col s4">
                    <p class="text-center">
                      <strong>Top 5 1st Diagnosis of those Readmitted within 30 Days</strong>
                    </p>

                    <div class="chart">
                      <!-- Sales Chart Canvas -->
                      <canvas id="question5_chart" style="padding-left: 35px; max-width: 95%;"></canvas>
                    </div>
                  </div>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <!-- /.box-body -->
          </div>


          <!-- <div class="row"> -->
            <div class="col-md-6 col-sm-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title"><a href="#" id="q6_table" href="#" data-toggle="modal" data-target="#modal-default">Top 5 1st Diagnosis Readmitted Before 30 Days and Grouped by Sex</a></h3>

                  <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col s4">
                      <p class="text-center">
                        <strong>Top 5 1st Diagnosis Readmitted before 30 days and Grouped by sex</strong>
                      </p>

                      <div class="chart">
                        <!-- Sales Chart Canvas -->
                        <canvas id="question6_chart" style="padding-left: 35px; max-width: 95%;"></canvas>
                      </div>
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- /.box-body -->
            </div>

            <!-- </div> -->
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Number of Patients Readmitted within 30 days Grouped by Admission Type Reasons</h3>

                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <div class="row">
                      <div class="col s4">
                        <p class="text-center">
                          <strong>Number of Patients Readmitted within 30 days grouped by Admission Type reasons</strong>
                        </p>

                        <div class="chart">
                          <!-- Sales Chart Canvas -->
                          <canvas id="question7_chart" style="padding-left: 35px; max-width: 95%;"></canvas>
                        </div>
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-body -->
              </div>
              <div class="col-md-6 col-sm-12">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">Number of Patients Readmitted within 30 days with a 1st Diagnosis of Circulatory, Neoplasm, Respiratory, Digestive, Diabetes</a></h3>
                    <div class="box-tools pull-right">
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                      </button>
                      <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                    </div>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body no-padding">
                    <div class="row">
                      <div class="col s4">
                        <p class="text-center">
                          <strong>Number of Patients Readmitted within 30 days with a 1st Diagnosis of Circulatory, Neoplasm, Respiratory, Digestive, Diabetes</strong>
                        </p>

                        <div class="chart">
                          <!-- Sales Chart Canvas -->
                          <canvas id="question8_chart" style="padding-left: 35px; max-width: 95%;"></canvas>
                        </div>
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->
                </div>
                <!-- /.box-body -->
              </div>
            </div>      
          </div>

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- Main Footer -->
      <footer class="main-footer">
        <!-- Default to the left -->
        <strong>Copyright &copy; 2018 <a href="#">Mansilla, Perez, Sarmiento</a>.</strong> All rights reserved.
      </footer>
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 3 -->
    <script src="<?=base_url();?>/assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?=base_url();?>/assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url();?>/assets/adminlte/dist/js/adminlte.min.js"></script>
    <!-- ChartJS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
   </body>

   <script src="<?=base_url('assets/js/chartColors.js')?>"></script>

   <script>
     $.ajax({
      url: "<?=site_url() . 'data/get_question_1_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var value = [];

        data.forEach(function(obj) {
          value.push(obj.Count);
        });

        var chartdata = {
          labels: ['Not Readmitted Patients','Readmitted Patients'],
          datasets : [
          {
            data: value,
            backgroundColor: [window.chartColors.yellow,window.chartColors.orange]
          }
          ]
        };

        var ctx = $("#question1_chart");

        var pieGraph = new Chart(ctx, {
          type: 'doughnut',
          data: chartdata,
          options: {
            title: {
              display: false
            },
          }

        });
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
  </script>

  <script>
   $.ajax({
    url: "<?=site_url() . 'data/get_question_2_json';?>",
    method: "GET",
    data: ({}),
    dataType: 'json',
    success: function(data) {
      console.log("AJAX SUCCESS");
      console.log(data);
      var sex = [];
      var readmitted = [];
      var rowData = [];
      data.forEach(function(obj){
        if(obj.Sex == ""){
          if(sex.indexOf("Unspecified") == -1) sex.push("Unspecified");
        }else{
          if(sex.indexOf(obj.Sex) == -1) sex.push(obj.Sex);  
        }
        if(readmitted.indexOf(obj.Readmitted_within_30Days) == -1) readmitted.push(obj.Readmitted_within_30Days);
        var tempArray = [];
        if(obj.Sex == "") tempArray.push("Unspecified");
        else tempArray.push(obj.Sex);
        tempArray.push(obj.Readmitted_within_30Days);
        tempArray.push(obj.Count);
        rowData.push(tempArray);
      });
      // console.log(rowData);
      backgroundColor = [window.chartColors.green1,window.chartColors.green2,window.chartColors.green3];
      var jsonText = "[";
      for (var i = 0; i < sex.length; i++) {
        jsonText += "{\"label\": \"" + sex[i] + "\", \"data\": [";
        var dataPerSex = [];
        for (var j = 0; j < rowData.length; j++) {
          if(rowData[j][0] == sex[i]) dataPerSex.push("\"" + rowData[j][2] + "\"");
        }
        // jsonText += dataPerSex + "], \"backgroundColor\": " +  + "}";
        jsonText += dataPerSex + "], \"backgroundColor\": [";
        for(var k=0; k < 2; k++){
          jsonText += "\"" + backgroundColor[i] + "\"";
          if(k < 1) jsonText += ",";
        }
        jsonText += "]}";
        // if(i<method.length-1) jsonText += ",";
        if(i<sex.length-1) jsonText += ",";
      }
      jsonText += "]";
      var jsonParsed = JSON.parse(jsonText);
      var chartdata = {
        labels: ["Patients Not Readmitted Within 30 Days","Patients Readmitted Within 30 Days"],
        datasets: jsonParsed 
      };

      var ctx = $("#question2_chart");

      var barGraph = new Chart(ctx, {
        type: 'bar',
        data: chartdata,
        options: {
          title: {
            display: false
          },
          tooltips: {
            mode: 'index',
            intersect: false
          },
          responsive: true,
          scales: {
            xAxes: [{
              stacked: true
            }],
            yAxes: [{
              stacked: true
            }]
          }
        }

      });
      console.log(barGraph);
    },
    error: function(data) {
      console.log("AJAX ERROR");
      console.log(data);
    }
  });
</script>

<script>
 $.ajax({
  url: "<?=site_url() . 'data/get_question_3_json';?>",
  method: "GET",
  data: ({}),
  dataType: 'json',
  success: function(data) {
    console.log("AJAX SUCCESS");
    console.log(data);
      var age_group = [];  //Ito mamaya yung magiging labels
      var count = []; //Ito yung actual bars sa graph

      data.forEach(function(obj){ //DITO NATIN SIYA GAGAWIN
        if(obj.Age_Group == null){
          age_group.push("Null");        
        }else age_group.push(obj.Age_Group);
        count.push(obj.Count);
      });

      var chartdata = {
      labels: age_group, //X-axis labels
      datasets:[
      {
            label: 'Count', //Pag minouse-over mo yung bar ito lalabas
            backgroundColor: window.chartColors.red,             
            data: count
          }
          ]
        };

        console.log(chartdata);

        var ctx = $("#question3_chart");

        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata,
          options: {
            title: {
              display: false
            },
          }
        });
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
  </script>

  <script>
   $.ajax({
    url: "<?=site_url() . 'data/get_question_4_json';?>",
    method: "GET",
    data: ({}),
    dataType: 'json',
    success: function(data) {
      console.log("AJAX SUCCESS");
      console.log(data);
      var time_in_hospital = [];  //Ito mamaya yung magiging labels
      var count = []; //Ito yung actual bars sa graph

      data.forEach(function(obj){ //DITO NATIN SIYA GAGAWIN
        if(obj.time_in_hospital == null){
          time_in_hospital.push("Null");        
        }else time_in_hospital.push(obj.time_in_hospital);
        count.push(obj.Count);
      });

      var chartdata = {
      labels: time_in_hospital, //X-axis labels
      datasets:[
      {
            label: 'Count', //Pag minouse-over mo yung bar ito lalabas
            backgroundColor: window.chartColors.blue,
            data: count
          }
          ]
        };

        console.log(chartdata);

        var ctx = $("#question4_chart");

        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata,
          options: {
            title: {
              display: false
            },
          }
        });
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
  </script>

  <script>
   $.ajax({
    url: "<?=site_url() . 'data/get_question_5_json';?>",
    method: "GET",
    data: ({}),
    dataType: 'json',
    success: function(data) {
      console.log("AJAX SUCCESS");
      console.log(data);
      var Group_name = [];  //Ito mamaya yung magiging labels
      var Number_of_Cases = []; //Ito yung actual bars sa graph

      data.forEach(function(obj){ //DITO NATIN SIYA GAGAWIN
        if(obj.Group_name == null){
          Group_name.push("Null");        
        }else Group_name.push(obj.Group_name);
        Number_of_Cases.push(obj.Number_of_Cases);
      });

      var chartdata = {
      labels: Group_name, //X-axis labels
      datasets:[
      {
            label: 'Count', //Pag minouse-over mo yung bar ito lalabas
            backgroundColor: window.chartColors.blue1,
            data: Number_of_Cases
          }
          ]
        };

        console.log(chartdata);

        var ctx = $("#question5_chart");

        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata,
          options: {
            title: {
              display: false
            },
          }
        });
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
  </script>

  <script>
    $("#q1_table").on("click",function(){
     $.ajax({
      url: "<?=site_url() . 'data/get_question_1_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      beforeSend: function(){
        $('#modal_loader').show();
        $('#data').hide();
      },
      complete: function(){
        $('#modal_loader').hide();
        $('#data').show();
      },
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var headers = Object.keys(data[0]);
        var value = [];

        var tableHTML = "<tr>";
        for (var i = 0; i < headers.length; i++) {
          tableHTML += "<th>" + headers[i] + "</th>";
        }
        tableHTML += "</tr>";
        data.forEach(function(obj) {
          tableHTML += "<tr>";
          for (var i = 0; i < headers.length; i++) {
            tableHTML += "<td>" + obj[headers[i]] + "</td>";
          }
          tableHTML += "</tr>";
        });
        $("#data").html(tableHTML);
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
   });

    $("#q2_table").on("click",function(){
     $.ajax({
      url: "<?=site_url() . 'data/get_question_2_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      beforeSend: function(){
        $('#modal_loader').show();
        $('#data').hide();
      },
      complete: function(){
        $('#modal_loader').hide();
        $('#data').show();
      },
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var headers = Object.keys(data[0]);
        var value = [];

        var tableHTML = "<tr>";
        for (var i = 0; i < headers.length; i++) {
          tableHTML += "<th>" + headers[i] + "</th>";
        }
        tableHTML += "</tr>";
        data.forEach(function(obj) {
          tableHTML += "<tr>";
          for (var i = 0; i < headers.length; i++) {
            tableHTML += "<td>" + obj[headers[i]] + "</td>";
          }
          tableHTML += "</tr>";
        });
        $("#data").html(tableHTML);
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
   });

    $("#q3_table").on("click",function(){
     $.ajax({
      url: "<?=site_url() . 'data/get_question_3_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      beforeSend: function(){
        $('#modal_loader').show();
        $('#data').hide();
      },
      complete: function(){
        $('#modal_loader').hide();
        $('#data').show();
      },
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var headers = Object.keys(data[0]);
        var value = [];

        var tableHTML = "<tr>";
        for (var i = 0; i < headers.length; i++) {
          tableHTML += "<th>" + headers[i] + "</th>";
        }
        tableHTML += "</tr>";
        data.forEach(function(obj) {
          tableHTML += "<tr>";
          for (var i = 0; i < headers.length; i++) {
            tableHTML += "<td>" + obj[headers[i]] + "</td>";
          }
          tableHTML += "</tr>";
        });
        $("#data").html(tableHTML);
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
   });

    $("#q4_table").on("click",function(){
     $.ajax({
      url: "<?=site_url() . 'data/get_question_4_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      beforeSend: function(){
        $('#modal_loader').show();
        $('#data').hide();
      },
      complete: function(){
        $('#modal_loader').hide();
        $('#data').show();
      },
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var headers = Object.keys(data[0]);
        var value = [];

        var tableHTML = "<tr>";
        for (var i = 0; i < headers.length; i++) {
          tableHTML += "<th>" + headers[i] + "</th>";
        }
        tableHTML += "</tr>";
        data.forEach(function(obj) {
          tableHTML += "<tr>";
          for (var i = 0; i < headers.length; i++) {
            tableHTML += "<td>" + obj[headers[i]] + "</td>";
          }
          tableHTML += "</tr>";
        });
        $("#data").html(tableHTML);
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
   });

    $("#q5_table").on("click",function(){
     $.ajax({
      url: "<?=site_url() . 'data/get_question_5_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      beforeSend: function(){
        $('#modal_loader').show();
        $('#data').hide();
      },
      complete: function(){
        $('#modal_loader').hide();
        $('#data').show();
      },
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var headers = Object.keys(data[0]);
        var value = [];

        var tableHTML = "<tr>";
        for (var i = 0; i < headers.length; i++) {
          tableHTML += "<th>" + headers[i] + "</th>";
        }
        tableHTML += "</tr>";
        data.forEach(function(obj) {
          tableHTML += "<tr>";
          for (var i = 0; i < headers.length; i++) {
            tableHTML += "<td>" + obj[headers[i]] + "</td>";
          }
          tableHTML += "</tr>";
        });
        $("#data").html(tableHTML);
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
   });

        $("#q6_table").on("click",function(){
     $.ajax({
      url: "<?=site_url() . 'data/get_question_6_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      beforeSend: function(){
        $('#modal_loader').show();
        $('#data').hide();
      },
      complete: function(){
        $('#modal_loader').hide();
        $('#data').show();
      },
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var headers = Object.keys(data[0]);
        var value = [];

        var tableHTML = "<tr>";
        for (var i = 0; i < headers.length; i++) {
          tableHTML += "<th>" + headers[i] + "</th>";
        }
        tableHTML += "</tr>";
        data.forEach(function(obj) {
          tableHTML += "<tr>";
          for (var i = 0; i < headers.length; i++) {
            tableHTML += "<td>" + obj[headers[i]] + "</td>";
          }
          tableHTML += "</tr>";
        });
        $("#data").html(tableHTML);
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
   });

    $.ajax({
      url: "<?=site_url() . 'data/get_question_6_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
        var group_name = [];
        var sex = [];
        var Number_of_Cases = [];
        var rowData = [];
        data.forEach(function(obj){
          if(obj.Sex == ""){
            if(sex.indexOf("Unspecified") == -1) sex.push("Unspecified");
          }else{
            if(sex.indexOf(obj.Sex) == -1) sex.push(obj.Sex);  
          }
          if(group_name.indexOf(obj.Group_name) == -1) group_name.push(obj.Group_name);
          var tempArray = [];
          if(obj.Sex == "") tempArray.push("Unspecified");
          else tempArray.push(obj.Sex);
          tempArray.push(obj.Group_name);
          tempArray.push(obj.Number_of_Cases);
          rowData.push(tempArray);
        });
        var bgColors = ["\"#ff0000\"", "\"#00ff00\"", "\"#0000ff\"", "\"#f0000f\"", "\"#0f0f0f\"", "\"#f0f0f0\""];
        console.log(rowData);
        var jsonText = "[";
        for (var i = 0; i < sex.length; i++) {
          jsonText += "{\"label\": \"" + sex[i] + "\", \"data\": [";
          var dataPerSex = [];
          for (var j = 0; j < rowData.length; j++) {
            if(rowData[j][0] == sex[i]) dataPerSex.push("\"" + rowData[j][2] + "\"");
          }
          jsonText += dataPerSex + "], \"backgroundColor\": " + bgColors[i] + "}";
          if(i<sex.length-1) jsonText += ",";
        }
        jsonText += "]";
        var jsonParsed = JSON.parse(jsonText);
        var chartdata = {
          labels: group_name,
          datasets: jsonParsed 
        };

        var ctx = $("#question6_chart");

        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata,
          options: {
            title: {
              display: false
            },
            tooltips: {
              mode: 'index',
              intersect: false
            },
            responsive: true,
            scales: {
              xAxes: [{
                stacked: true
              }],
              yAxes: [{
                stacked: true
              }]
            }
          }

        });
        console.log(barGraph);
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });

    $.ajax({
      url: "<?=site_url() . 'data/get_question_7_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
    var Description = []; //Ito mamaya yung magiging labels
      var Count = [];  //Ito yung actual bars sa graph

      data.forEach(function(obj){ //DITO NATIN SIYA GAGAWIN
        if(obj.Description == null){
          Description.push("Null");        
        }else Description.push(obj.Description);
        Count.push(obj.Count);
      });

      var chartdata = {
      labels: Description, //X-axis labels
      datasets:[
      {
            label: 'Number of Patients', //Pag minouse-over mo yung bar ito lalabas
            backgroundColor: 'rgba(255, 99, 1, 1)',
            borderColor: 'rgba(255, 99, 132, 1)',
            hoverBackgroundColor: 'rgba(255, 125, 132, 1)',
            hoverBorderColor: 'rgba(255, 125, 132, 1)',
            data: Count
          }
          ]
        };

        console.log(chartdata);

        var ctx = $("#question7_chart");

        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata,
          options: {
            title: {
              display: false
            },
          }
        });
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });

    $.ajax({
      url: "<?=site_url() . 'data/get_question_8_json';?>",
      method: "GET",
      data: ({}),
      dataType: 'json',
      success: function(data) {
        console.log("AJAX SUCCESS");
        console.log(data);
    var Group_name = []; //Ito mamaya yung magiging labels
      var Count = [];  //Ito yung actual bars sa graph

      data.forEach(function(obj){ //DITO NATIN SIYA GAGAWIN
        if(obj.Group_name == null){
          Group_name("Null");        
        }else Group_name.push(obj.Group_name);
        Count.push(obj.Count);
      });

      var chartdata = {
      labels: Group_name, //X-axis labels
      datasets:[
      {
            label: 'Number of Patients', //Pag minouse-over mo yung bar ito lalabas
            backgroundColor: 'rgba(255, 99, 132, 1)',
            borderColor: 'rgba(255, 99, 132, 1)',
            hoverBackgroundColor: 'rgba(255, 125, 132, 1)',
            hoverBorderColor: 'rgba(255, 125, 132, 1)',
            data: Count
          }
          ]
        };

        console.log(chartdata);

        var ctx = $("#question8_chart");

        var barGraph = new Chart(ctx, {
          type: 'bar',
          data: chartdata,
          options: {
            title: {
              display: false
            },
          }
        });
      },
      error: function(data) {
        console.log("AJAX ERROR");
        console.log(data);
      }
    });
  </script>

  <!-- FUNCTION FOR 6 --> 


  </html>
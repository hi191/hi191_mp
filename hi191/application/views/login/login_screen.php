<!DOCTYPE html>
<html>
<head>
	<title>DrugComMon | Log-in</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/paper/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src = "//code.jquery.com/jquery-1.12.4.js"></script>
	<script src ="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script src = "https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
	<link rel="stylesheet" href="<?=base_url()?>assets/css/login.css">
	<link rel="shortcut icon" href="<?=base_url()?>assets/images/logo.png">

</head>
<body>
	<div class="container contain">
		<div class="panel contentBox container-fluid" id="Login" >
			<div class="panel-content" id="LoginForm">
				<img id="logo" class="center-block img-responsive" src="<?=base_url()?>/assets/images/login.png" alt="logo">
				<div class="loginf">
					<?php
					if(isset($_SESSION['Exist'])){
						echo '<div class="alert alert-dismissible alert-danger" id="login-fail" style="display:block;">';
					}else{
						echo '<div class="alert alert-dismissible alert-danger" id="login-fail" style="display:none;">';
					}
					?>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					<strong>User not found.</strong> 
					Employee ID or Password may be incorrect.
				</div>	
				<?php echo form_open('Login/login_user', 'class="form-horizontal"'); ?>
				<fieldset>
					<div class="form-group container-fluid">
						<label class="control-label" for="email">Username</label>
						<input type="text" class="form-control col-sm-4" name="employeeID" required>	
					</div>
					<div class="form-group container-fluid">
						<label class="control-label" for="password">Password</label>
						<input type="password" class="form-control col-sm-4" name="inputPassword" required>	
					</div>
					<div class="form-group container-fluid row">
						<div class="col-xs-6 col-xs-offset-3">
							<div class="text-center">
								<button type="reset" class="btn btn-default">Reset</button>
								<button type="submit" class="btn btn-primary">Log-in</button>
								<!-- <button type="button" class="btn btn-primary" onclick="showModal()">Test</button> -->
							</div>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
</body>
<script>
		//beyond this bago na
		$(document).ready(function() {
			$('#patientTable').dataTable();
		});
	</script>
	<?php if(isset($_SESSION['Exist'])){
		echo'<style>
				#Login {
		display:block;
		max-height: 800px;
	}
	</style>';
	echo 
	"<script>
	function showBox(id){
		if(id == 0){
			document.getElementById('LoginForm').style.display = 'none';
			document.getElementById('pharmacyTable').style.display = 'block';
			document.getElementById('Login').style.maxWidth='850px';
			document.getElementById('Login').style.maxHeight='1250px';
		}else if(id == 1){
			document.getElementById('LoginForm').style.display = 'block';
			document.getElementById('pharmacyTable').style.display = 'none';
			document.getElementById('Login').style.maxWidth='450px';
			document.getElementById('Login').style.maxHeight='800px';
		}
	}
	</script>";
}else{
	echo'<style>
				#Login {
	display:block;
	max-height: 600x;
}
</style>';
echo 
"<script>
function showBox(id){
	if(id == 0){
		document.getElementById('LoginForm').style.display = 'none';
		document.getElementById('Login').style.maxWidth='850px';
		document.getElementById('Login').style.maxHeight='1250px';
	}else if(id == 1){
		document.getElementById('LoginForm').style.display = 'block';
		document.getElementById('Login').style.maxWidth='450px';
		document.getElementById('Login').style.maxHeight='600px';
	}
}
</script>";
}?>

<script>
	function showModal(){
		document.getElementById('login-fail').style.display = 'block';
	}
</script>

<script>
	function isPasswordMatch() {
		var password = $("#rpassword").val();
		var confirmPassword = $("#repassword").val();

		if (password != confirmPassword){
			$("#divCheckPassword").html("Passwords do not match!");
			$(':input[type="submit"]').prop('disabled', true);
		}
		else{
			$("#divCheckPassword").html("");
			$(':input[type="submit"]').prop('disabled', false);
		}
	}

	$(document).ready(function () {
		$("#txtConfirmPassword").keyup(isPasswordMatch);
	});
</script>
</body>
</html>

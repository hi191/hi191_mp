  <head>
    <link rel="shortcut icon" href="<?=base_url();?>assets/images/logo.png">
  </head>

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVIGATION</li>
        <!-- Optionally, you can add icons to the links -->
        <?php if($nav == "Dashboard"):?>
        	<li class="active">
            <?php else:?>
             <li>
             <?php endif?>
             <a href="<?=site_url()?>dashboard"><i class="fa fa-tachometer" aria-hidden="true"></i> <span>Dashboard</span></a></li>
           </li>

           <!-- /.sidebar-menu -->
         </section>
         <!-- /.sidebar -->
       </aside>
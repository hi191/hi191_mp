<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_model extends CI_Model {

	public function question1(){
		$fields = "`Readmitted_within_30Days`, COUNT(`Readmitted_within_30Days`) AS 'Count'";
		$this->db->select($fields,FALSE)
		->from("all_readmissions")
		->group_start()
		->where("Readmitted_within_30Days", '1')
		->or_where("Readmitted_within_30Days", '0')
		->group_end()
		->group_by("Readmitted_within_30Days");
		return $this->db->get()->result();
	}

	public function question2(){
		$fields = "`gender` AS 'Sex', `Readmitted_within_30Days`, COUNT(*) AS 'Count'";
		$this->db->select($fields,FALSE)
		->from("all_readmissions_by_gender")
		->group_start()
		->where("Readmitted_within_30Days", '1')
		->or_where("Readmitted_within_30Days", '0')
		->group_end()
		->group_by(array("gender","Readmitted_within_30Days"))
		->order_by("Readmitted_within_30Days");
		return $this->db->get()->result();
	}

	public function question3(){
		$fields = "`age` AS 'Age_Group',
		COUNT(*) AS 'Count'";
		$this->db->select($fields,FALSE)
		->from("patients_by_age_group")
		->group_by("age");
		return $this->db->get()->result();
	}

	public function question4(){
		$fields = "*, COUNT(*) AS 'Count'";
		$this->db->select($fields,FALSE)
		->from("number_of_days_confined")
		->group_by("time_in_hospital")
		->order_by("time_in_hospital");
		return $this->db->get()->result();
	}

	public function question5(){
		$fields = "`Group_name`, 
			COUNT(*) AS 'Number_of_Cases'";
		$this->db->select($fields,FALSE)
		->from("group_1_of_readmitted")
		->group_by("Group_name")
		->order_by('Number_of_Cases', 'DESC')
		->limit(5, 0);
		return $this->db->get()->result();
	}

	public function question6(){
		$fields = "`Group_name` AS 'Group_name', 
			`gender` AS 'Sex', COUNT(*) AS 'Number_of_Cases' ";
		$this->db->select($fields,FALSE)
		->from("group_1_of_readmitted_by_gender")
		->where_in("Group_name", array("Circulatory", "Neoplasms", "Respiratory", "Diabetes", "Digestive"))
		->group_by(array("Group_name","gender"))
		->order_by('Number_of_Cases', 'gender', 'ASC')
		->limit(10, 0);
		return $this->db->get()->result();
	}
	
	public function question7(){
		$fields = "`AT_Descr` AS 'Description', COUNT(*) AS 'Count'";
		$this->db->select($fields,FALSE)
		->from("admission_type_em_ur_el")
		->group_by("AT_Descr")
		->order_by("AT_Descr");
		return $this->db->get()->result();
		// var_dump($this->db->get()->result());
	}
	
	public function question8(){
		$fields = "`Group_name` AS 'Group_name', COUNT(*) AS 'Count'";
		$this->db->select($fields,FALSE)
		->from("admission_type_em_ur_el_with_group_diag1")
		->where_in("Group_name", array("Circulatory","Neoplasms","Respiratory","Digestive","Diabetes"))
		->group_by("Group_name")
		->order_by('COUNT(*)');
		return $this->db->get()->result();
		// var_dump($this->db->get()->result());
	}
}

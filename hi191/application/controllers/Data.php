<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data extends CI_Controller {
	public function index(){
		
	}

	public function get_question_1_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question1();
			echo json_encode($res);
		}
	}

	public function get_question_2_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question2();
			echo json_encode($res);
		}
	}

	public function get_question_3_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question3();
			echo json_encode($res);
		}
	}

	public function get_question_4_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question4();
			echo json_encode($res);
		}
	}

	public function get_question_5_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question5();
			echo json_encode($res);
		}
	}

	public function get_question_6_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question6();
			echo json_encode($res);
		}
	}

	public function get_question_7_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question7();
			echo json_encode($res);
		}
	}

	public function get_question_8_json(){
		if(isset($_REQUEST)){
			$res = $this->data->question8();
			echo json_encode($res);
		}
	}
}

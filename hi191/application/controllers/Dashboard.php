<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function index(){
		$nav['nav'] = "Dashboard";
		$this->load->view('dashboard.php');
		$this->load->view('sidebar/sidebar.php', $nav);
		
	}
}
